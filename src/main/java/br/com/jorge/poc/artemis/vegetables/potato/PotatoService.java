package br.com.jorge.poc.artemis.vegetables.potato;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface PotatoService {
    Potato savePotato(Potato potato);
    Potato findPotatoById(String id);
    Page<Potato> findAllPotato(Pageable pageable);
}
