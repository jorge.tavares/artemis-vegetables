package br.com.jorge.poc.artemis.vegetables.integration.potato;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@AllArgsConstructor
public class PotatoConsumer {

    @JmsListener(destination = "${artemis.queue.potato-create}", containerFactory = "defaultFactory")
    public void onReceiveMessage(PotatoCreateEvent potatoCreateEvent) {
        log.info("Potato create event received: " + potatoCreateEvent);
    }
}
