package br.com.jorge.poc.artemis.vegetables.potato.api;

import br.com.jorge.poc.artemis.vegetables.potato.Potato;
import br.com.jorge.poc.artemis.vegetables.potato.PotatoService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.HttpStatus.*;

@RestController
@AllArgsConstructor
@RequestMapping(value = "/vegetables/potatos")
class PotatoRestService {

    private final PotatoService potatoService;

    @PostMapping
    @ResponseStatus(CREATED)
    Potato create(@RequestBody Potato potato) {
        return potatoService.savePotato(potato);
    }

    @GetMapping("/{id}")
    @ResponseStatus(OK)
    Potato getById(@PathVariable String id) {
        return potatoService.findPotatoById(id);
    }

    @GetMapping
    @ResponseStatus(OK)
    Page<Potato> getAll(@RequestParam(defaultValue = "0", required = false) Integer page,
                        @RequestParam(defaultValue = "20", required = false) Integer size) {
        return potatoService.findAllPotato(PageRequest.of(page, size));
    }
}
