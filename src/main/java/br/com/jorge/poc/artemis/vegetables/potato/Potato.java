package br.com.jorge.poc.artemis.vegetables.potato;

import br.com.jorge.poc.artemis.vegetables.integration.potato.PotatoCreateEvent;
import lombok.*;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoId;

import static lombok.AccessLevel.PRIVATE;

@Getter
@Builder
@EqualsAndHashCode
@AllArgsConstructor
@Document(collection = "potato")
@NoArgsConstructor(access = PRIVATE)
public class Potato {

    @MongoId
    private String uuid;

    private String description;

    public static Potato of(PotatoCreateEvent potatoCreateEvent) {
        return Potato.builder()
                .uuid(potatoCreateEvent.getPotatoId())
                .description(potatoCreateEvent.getPotatoId())
                .build();
    }
}
