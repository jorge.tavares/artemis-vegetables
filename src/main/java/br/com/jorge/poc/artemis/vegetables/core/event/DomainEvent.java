package br.com.jorge.poc.artemis.vegetables.core.event;

import com.fasterxml.jackson.annotation.JsonIgnore;

public interface DomainEvent {
    @JsonIgnore
    String getEventType();
}
