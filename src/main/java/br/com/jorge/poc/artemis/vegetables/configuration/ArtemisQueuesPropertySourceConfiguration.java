package br.com.jorge.poc.artemis.vegetables.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:queues.properties")
public class ArtemisQueuesPropertySourceConfiguration {
}
