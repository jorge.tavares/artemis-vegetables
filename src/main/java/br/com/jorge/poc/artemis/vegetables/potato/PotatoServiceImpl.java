package br.com.jorge.poc.artemis.vegetables.potato;

import br.com.jorge.poc.artemis.vegetables.core.VegetablesMessageSender;
import br.com.jorge.poc.artemis.vegetables.core.exception.ResourceNotFoundException;
import br.com.jorge.poc.artemis.vegetables.core.property.ArtemisQueueProperty;
import br.com.jorge.poc.artemis.vegetables.integration.potato.PotatoCreateEvent;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@AllArgsConstructor
class PotatoServiceImpl implements PotatoService {

    private final PotatoRepository potatoRepository;
    private final VegetablesMessageSender<PotatoCreateEvent> vegetablesMessageSender;
    private final ArtemisQueueProperty artemisQueueProperty;

    @Override
    @Transactional
    public Potato savePotato(Potato potato) {
        potato = potatoRepository.save(potato);
        vegetablesMessageSender.send(artemisQueueProperty.getPotatoCreate(), PotatoCreateEvent.of(potato));
        return potato;
    }

    @Override
    public Potato findPotatoById(String id) {
        return potatoRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Potato not found for " + id));
    }

    @Override
    public Page<Potato> findAllPotato(Pageable pageable) {
        return potatoRepository.findAll(pageable);
    }

}
