package br.com.jorge.poc.artemis.vegetables.core;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@AllArgsConstructor
public class VegetablesMessageSender<T> {

    private final JmsTemplate jmsTemplate;

    public void send(String destination, T event) {
        log.info("Sending event to: " + destination);
        jmsTemplate.convertAndSend(destination, event);
    }

}
