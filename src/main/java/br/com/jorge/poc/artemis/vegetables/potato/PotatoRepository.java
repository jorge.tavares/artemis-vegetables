package br.com.jorge.poc.artemis.vegetables.potato;

import org.springframework.data.mongodb.repository.MongoRepository;

interface PotatoRepository extends MongoRepository<Potato, String> {}
