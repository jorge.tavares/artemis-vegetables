package br.com.jorge.poc.artemis.vegetables.core.property;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "artemis.queue")
public class ArtemisQueueProperty {
    private String potatoCreate;
}
