package br.com.jorge.poc.artemis.vegetables.tomato;

import lombok.*;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoId;

import static lombok.AccessLevel.PRIVATE;

@Getter
@Builder
@EqualsAndHashCode
@AllArgsConstructor
@Document(collection = "tomato")
@NoArgsConstructor(access = PRIVATE)
public class Tomato {

    @MongoId
    private String uuid;

    private String description;
}
