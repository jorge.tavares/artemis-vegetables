package br.com.jorge.poc.artemis.vegetables.integration.potato;

import br.com.jorge.poc.artemis.vegetables.core.event.DomainEvent;
import br.com.jorge.poc.artemis.vegetables.potato.Potato;
import lombok.*;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@ToString
@AllArgsConstructor
@Builder(toBuilder = true)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class PotatoCreateEvent implements DomainEvent {

    private String potatoId;
    private String description;

    @Override
    public String getEventType() {
        return this.getClass().getSimpleName();
    }

    public static PotatoCreateEvent of(Potato potato) {
        return PotatoCreateEvent.builder()
                .potatoId(potato.getUuid())
                .description(potato.getDescription())
                .build();
    }

}
