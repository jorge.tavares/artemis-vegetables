package br.com.jorge.poc.artemis.vegetables;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class PocArtemisVegetablesApplicationTests {

	@Test
	public void contextLoads() {
	}

}
