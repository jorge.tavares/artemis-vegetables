package br.com.jorge.poc.artemis.vegetables.potato.api


import br.com.jorge.poc.artemis.vegetables.potato.Potato
import br.com.jorge.poc.artemis.vegetables.potato.PotatoService
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import spock.lang.Shared
import spock.lang.Specification

class PotatoRestServiceTest extends Specification {

    @Shared
    Potato potato

    @Shared
    PotatoService potatoGateway


    def setup() {
        potato = Potato.builder()
                .uuid(UUID.fromString('3d34e890-7fb5-4ea7-bf40-c5dd2073467c').toString())
                .description("Im a a potato o/")
                .build()
        potatoGateway = Mock(PotatoService)
    }

    def 'should create a potato with success'() {
        given:
        PotatoRestService potatoRestService = new PotatoRestService(potatoGateway)

        when:
        def result = potatoRestService.create(potato)

        then:
        1 * potatoGateway.savePotato(potato) >> potato

        and:
        assert result.uuid == potato.uuid
        assert result.description == potato.description
    }

    def 'should get one potato by id with success'() {
        given:
        PotatoRestService potatoRestService = new PotatoRestService(potatoGateway)

        when:
        def result = potatoRestService.getById('3d34e890-7fb5-4ea7-bf40-c5dd2073467c')

        then:
        1 * potatoGateway.findPotatoById('3d34e890-7fb5-4ea7-bf40-c5dd2073467c') >> potato

        and:
        assert result.uuid == potato.uuid
        assert result.description == potato.description
    }

    def 'should retrieve the potato first page with success' () {
        given:
        PotatoRestService potatoRestService = new PotatoRestService(potatoGateway)
        Pageable firstPage = PageRequest.of(0, 1)

        when:
        def result = potatoRestService.getAll(0, 1)

        then:
        1 * potatoGateway.findAllPotato(firstPage) >> new PageImpl<>(Arrays.asList(potato))

        and:
        assert result.totalElements == 1
        assert result.content.get(0).uuid == potato.uuid
        assert result.content.get(0).description == potato.description
    }
}
