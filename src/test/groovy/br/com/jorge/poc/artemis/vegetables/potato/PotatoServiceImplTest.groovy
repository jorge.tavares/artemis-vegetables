package br.com.jorge.poc.artemis.vegetables.potato

import br.com.jorge.poc.artemis.vegetables.core.VegetablesMessageSender
import br.com.jorge.poc.artemis.vegetables.core.exception.ResourceNotFoundException
import br.com.jorge.poc.artemis.vegetables.core.property.ArtemisQueueProperty
import br.com.jorge.poc.artemis.vegetables.integration.potato.PotatoCreateEvent
import org.junit.Ignore
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import spock.lang.Shared
import spock.lang.Specification

@Ignore
class PotatoServiceImplTest extends Specification {

    @Shared
    Potato potato

    @Shared
    PotatoCreateEvent potatoCreateEvent

    @Shared
    PotatoRepository potatoRepository

    @Shared
    VegetablesMessageSender<PotatoCreateEvent> vegetablesMessageSender

    @Shared
    ArtemisQueueProperty artemisQueueProperty


    def setup() {
        potato = Potato.builder()
                .uuid(UUID.fromString('3d34e890-7fb5-4ea7-bf40-c5dd2073467c').toString())
                .description("Im a a potato o/")
                .build()
        potatoCreateEvent = PotatoCreateEvent.of(potato)
        potatoRepository = Mock(PotatoRepository)
        vegetablesMessageSender = Mock(VegetablesMessageSender)
        artemisQueueProperty = Mock(ArtemisQueueProperty)
    }

    def 'should save a potato with success' () {
        given:
        PotatoService potatoGateway = new PotatoServiceImpl(potatoRepository, vegetablesMessageSender, artemisQueueProperty)

        when:
        def result = potatoGateway.savePotato(potato)

        then:
        1 * potatoRepository.save(potato) >> potato
        1 * artemisQueueProperty.getPotatoCreate() >> 'potato-queue'
        1 * vegetablesMessageSender.send('potato-queue', potatoCreateEvent)

        and:
        assert result.uuid == potato.uuid
        assert result.description == potato.description
    }

    def 'should retrieve just one potato with success' () {
        given:
        PotatoService potatoGateway = new PotatoServiceImpl(potatoRepository, vegetablesMessageSender, artemisQueueProperty)

        when:
        def result = potatoGateway.findPotatoById('3d34e890-7fb5-4ea7-bf40-c5dd2073467c')

        then:
        1 * potatoRepository.findById('3d34e890-7fb5-4ea7-bf40-c5dd2073467c') >> Optional.of(potato)

        and:
        assert result.uuid == potato.uuid
        assert result.description == potato.description
    }

    def 'should throw a resource not found exception when potato id not exists' () {
        given:
        PotatoService potatoGateway = new PotatoServiceImpl(potatoRepository, vegetablesMessageSender, artemisQueueProperty)

        when:
        def result = potatoGateway.findPotatoById('3d34e890-7fb5-4ea7-bf40-c5dd2073467c')

        then:
        1 * potatoRepository.findById('3d34e890-7fb5-4ea7-bf40-c5dd2073467c') >> Optional.empty()

        and:
        ResourceNotFoundException e = thrown(ResourceNotFoundException)

        and:
        assert e.message == 'Potato not found for ' + '3d34e890-7fb5-4ea7-bf40-c5dd2073467c'
    }


    def 'should retrieve a list of potato with success' () {
        given:
        PotatoService potatoGateway = new PotatoServiceImpl(potatoRepository, vegetablesMessageSender, artemisQueueProperty)
        Pageable firstPage = PageRequest.of(0, 1)

        when:
        def result = potatoGateway.findAllPotato(firstPage)

        then:
        1 * potatoRepository.findAll(firstPage) >> new PageImpl<>(Arrays.asList(potato))

        and:
        assert result.totalElements == 1
        assert result.content.get(0).uuid == potato.uuid
        assert result.content.get(0).description == potato.description
    }

}
